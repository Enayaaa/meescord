<?php

// If class not set or not exist: redirect to home page
if (!isset($_GET["class"]) || !file_exists("../backend/classes/" . $_GET['class'] . ".json")) {
    header("Location: ../index.php");
}

//Load class json
$file = require $_SERVER["DOCUMENT_ROOT"] . "/backend/get_class.php";
$json = json_decode($file, true);
$title = "Meescord - " . $json["title"];
$flex = false;
require $_SERVER["DOCUMENT_ROOT"] . "/include/header.php";
?>
<main>
    <a href="/" class="btn btn-primary" role="button" style="margin-bottom: 1rem;">Tillbaka till
        kanaler</a>
    <h3 class="klasstitel">
        <?php echo $json["title"]; ?>
    </h3>
    <div class="kanal">
        <div class="overveiw">
            <ul class="list-group" id="room-list">
                <!-- Here will roomlist be entered! -->
                <?php foreach ($json["rooms"] as $key => $value) : ?>
                <a class="list-group-item list-group-item-action room-link" target="_blank"
                    href="<?php echo $value; ?>">
                    <div class="d-flex justify-content-between align-items-center">
                        <b>
                            <?php echo $key; ?>
                        </b>
                        <div><span class="badge badge-primary badge-pill">Google Meet</span><img src="/assets/meet.png"
                                id="meeticon"></div>
                    </div>
                </a>
                <?php endforeach; ?>
            </ul>
            <a class=" problem-link" href="/problem/">Problem med länk?</a>
        </div>
        <div class="content">

            <div class="accordion" id="accordionExample">
                <?php foreach ($json["classes"] as $key => $value) : ?>
                <?php $stripped = preg_replace('/\s/', '', $key); ?>
                <div class="card">
                    <div class="card-header" id="<?php echo 'Heading' . $stripped; ?>">
                        <h2 class="mb-0">
                            <button class="btn btn-link" type="button" data-toggle="collapse"
                                data-target="#<?php echo 'Collapse' . $stripped; ?>" aria-expanded="false"
                                aria-controls="<?php echo 'Collapse' . $stripped; ?>">
                                <?php echo $key; ?>
                            </button>
                        </h2>
                    </div>

                    <div id="<?php echo 'Collapse' . $stripped; ?>" class="collapse"
                        aria-labelledby="<?php echo 'Heading' . $stripped; ?>" data-parent="#accordionExample">
                        <div class="card-body">
                            <?php echo nl2br($value["info"]); ?>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>

    <script src="/main.js"></script>

</main>
<?php require $_SERVER["DOCUMENT_ROOT"] . "/include/footer.php"; ?>
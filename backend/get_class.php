<?php
$class = "";
if (isset($class_to_get)) {
    $class = $class_to_get;
} else {
    $class = $_GET["class"];
}
if ($class == "") {
    return "class empty error";
}
return file_get_contents($_SERVER["DOCUMENT_ROOT"]."/backend/classes/".$class.".json");
?>
<?php
$files = array_values(array_diff(scandir($_SERVER["DOCUMENT_ROOT"]."/backend/classes/"), array('..', '.')));
$dataArray = array();
for ($i = 0; $i < 0 + count($files); $i++) {
    $jsonString = file_get_contents($_SERVER["DOCUMENT_ROOT"]."/backend/classes/".$files[$i]);
    $json = json_decode($jsonString, true);
    
    $dataArray[$i]["title"] = $json["fname"]." ".$json["lname"];
    $dataArray[$i]["subjects"] = $json["subjects"];
    $dataArray[$i]["img"] = $json["img"];
    $dataArray[$i]["filename"] = substr($files[$i], 0, strlen($files[$i]) - 5);

}
return json_encode($dataArray);
?>

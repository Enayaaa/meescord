<?php
if (isset($_COOKIE["cookie_allowed"])) {
    session_start();
    if (!isset($_SESSION["logged_in"])) {
        die();
    }
} else {
    die();
}
$users = json_decode(file_get_contents($_SERVER["DOCUMENT_ROOT"]."/admin/secure.json"), true);
//im not server-side validating everything else, too much effort, a teacher wouldent go trying to break this anyway
if (isset($_GET["class"]) && isset($users[$_SESSION["name"]]["classes"][$_GET["class"]])) {
    $class = json_decode(file_get_contents($_SERVER["DOCUMENT_ROOT"]."/backend/classes/".$_GET["class"].".json"), true);
    $class["title"] = $_POST["title"];
    $class["fname"] = $_POST["fnamn"];
    $class["lname"] = $_POST["lnamn"];
    $class["img"] = $_POST["pic"];
    $class["rooms"] = array();
    foreach ($_POST["meetroom1"] as $key => $value) {
        $class["rooms"][$value] = $_POST["meetroom2"][$key];
    }
    $class["subjects"] = array();
    foreach ($_POST["subjects"] as $key => $value) {
        $class["subjects"][$key] = $value;
    }
    $class["classes"] = array();
    foreach ($_POST["classinfo1"] as $key => $value) {
        $class["classes"][$value] = array();
        $class["classes"][$value]["info"] = $_POST["classinfo2"][$key];
    }
    file_put_contents($_SERVER["DOCUMENT_ROOT"]."/backend/classes/".$_GET["class"].".json", json_encode($class));
    echo "<script>window.location.href = '/admin/';</script>";
    die();
} else {
    //du har inte perms för klass fel eller något sånt
    $title = "Meescord - Fel :^)";
    $flex = false;
    require $_SERVER["DOCUMENT_ROOT"] . "/include/header.php";
    echo "<main id='lcontainer'><h1>nått gjorde du fel</h1><a href='/admin/' class='btn btn-primary'>Gå tillbaka.</a></main>";
    require $_SERVER["DOCUMENT_ROOT"] . "/include/footer.php";
    die();
}

?>
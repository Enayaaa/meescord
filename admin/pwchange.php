<?php
if (isset($_COOKIE["cookie_allowed"])) {
    session_start();
    if (!isset($_SESSION["logged_in"])) {
        die();
    }
} else {
    die();
}
if (isset($_POST["pwchangenewpw"]) && isset($_POST["pwchangenewpw2"]) && $_POST["pwchangenewpw"] == $_POST["pwchangenewpw2"]) {
    $users = json_decode(file_get_contents($_SERVER["DOCUMENT_ROOT"]."/admin/secure.json"), true);
    $users[$_SESSION["name"]]["passhash"] = password_hash($_POST["pwchangenewpw"], PASSWORD_BCRYPT);
    file_put_contents($_SERVER["DOCUMENT_ROOT"]."/admin/secure.json", json_encode($users));
    session_destroy();
    setcookie("PHPSESSID","",time()-3600,"/");
    echo "<script>window.location.href = '/admin/';</script>"; //logga ut användaren, tvinga den att logga in igen för att testa lösen.
    die();
} else {
    $title = "Meescord - Fel :^)";
    $flex = false;
    require $_SERVER["DOCUMENT_ROOT"] . "/include/header.php";
    echo "<main id='lcontainer'><h1>Lösenord är inte likadana.</h1><a href='/admin/' class='btn btn-primary'>Gå tillbaka.</a></main>";
    require $_SERVER["DOCUMENT_ROOT"] . "/include/footer.php";
    die();
}
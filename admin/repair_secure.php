<?php
error_reporting(E_ERROR | E_PARSE); //page will throw an error if the file that we're repairing/creating doesnt exist... kinda pointless?

function repair() {
    $secure = array();
    $secure["admin"] = array();
    $secure["admin"]["passhash"] = password_hash("remilia", PASSWORD_BCRYPT); //default admin password is remilia
    $secure["admin"]["classes"] = array(); //just.. generate a empty array, lol
    $secure["admin"]["superadmin"] = true;
    file_put_contents($_SERVER["DOCUMENT_ROOT"] . "/admin/secure.json", json_encode($secure));
}

try {
    if (json_decode(file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/admin/secure.json")) == NULL) {
        repair();
    }
} catch (Exception $e) {
    repair();
}
?>
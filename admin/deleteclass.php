<?php
if (isset($_COOKIE["cookie_allowed"])) {
    session_start();
    if (!isset($_SESSION["logged_in"]) || $_SESSION["admin"] != true) {
        die();
    }
} else {
    die();
}
if (isset($_POST["class"])) {
    unlink($_SERVER["DOCUMENT_ROOT"]."/backend/classes/".$_POST["class"].".json");
    echo "<script>window.location.href = '/admin/';</script>";
} else {
    $title = "Meescord - Fel :^)";
    $flex = false;
    require $_SERVER["DOCUMENT_ROOT"] . "/include/header.php";
    echo "<main id='lcontainer'><h1>Klass ej vald?</h1><a href='/admin/' class='btn btn-primary'>Gå tillbaka.</a></main>";
    require $_SERVER["DOCUMENT_ROOT"] . "/include/footer.php";
    die();
}
?>
<?php
if (isset($_COOKIE["cookie_allowed"])) {
    session_start();
    if (!isset($_SESSION["logged_in"]) || $_SESSION["admin"] != true) {
        die();
    }
} else {
    die();
}
$users = json_decode(file_get_contents($_SERVER["DOCUMENT_ROOT"]."/admin/secure.json"), true);
if (isset($_POST["user"]) && isset($users[$_POST["user"]])) {
    $_SESSION["name"] = $_POST["user"];
    $_SESSION["admin"] = false;
    echo "<script>window.location.href = '/admin/';</script>";
} else {
    $title = "Meescord - Fel :^)";
    $flex = false;
    require $_SERVER["DOCUMENT_ROOT"] . "/include/header.php";
    echo "<main id='lcontainer'><h1>Användare finns inte.</h1><a href='/admin/' class='btn btn-primary'>Gå tillbaka.</a></main>";
    require $_SERVER["DOCUMENT_ROOT"] . "/include/footer.php";
    die();
}
?>
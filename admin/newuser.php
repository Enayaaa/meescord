<?php
if (isset($_COOKIE["cookie_allowed"])) {
    session_start();
    if (!isset($_SESSION["logged_in"]) || $_SESSION["admin"] != true) {
        die();
    }
} else {
    die();
}
if (isset($_POST["firstandlast"]) && isset($_POST["userpw"])) {
    $users = json_decode(file_get_contents($_SERVER["DOCUMENT_ROOT"]."/admin/secure.json"), true);
    $users[$_POST["firstandlast"]] = array();
    $users[$_POST["firstandlast"]]["passhash"] = password_hash($_POST["userpw"], PASSWORD_BCRYPT);
    $users[$_POST["firstandlast"]]["classes"] = array();
    $users[$_POST["firstandlast"]]["superadmin"] = (isset($_POST["isadmin"]) && $_POST["isadmin"] == true) ? true : false;
    file_put_contents($_SERVER["DOCUMENT_ROOT"]."/admin/secure.json", json_encode($users));
    echo "<script>window.location.href = '/admin/';</script>";
    die();
} else {
    $title = "Meescord - Fel :^)";
    $flex = false;
    require $_SERVER["DOCUMENT_ROOT"] . "/include/header.php";
    echo "<main id='lcontainer'><h1>nått gjorde du fel</h1><a href='/admin/' class='btn btn-primary'>Gå tillbaka.</a></main>";
    require $_SERVER["DOCUMENT_ROOT"] . "/include/footer.php";
    die();
}
?>
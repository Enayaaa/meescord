<?php
if (isset($_COOKIE["cookie_allowed"])) {
    session_start();
} else {
    echo "<script>window.location.href = '/admin/';</script>";
    die();
}
$error = "none! :D";

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $users = json_decode(file_get_contents($_SERVER["DOCUMENT_ROOT"]."/admin/secure.json"), true);
    if (isset($_POST["user"]) && isset($users[$_POST["user"]])) {
        if (isset($_POST["password"]) && password_verify($_POST["password"], $users[$_POST["user"]]["passhash"])) {
            $_SESSION["logged_in"] = true;
            $_SESSION["name"] = $_POST["user"];
            $_SESSION["admin"] = $users[$_POST["user"]]["superadmin"];
            echo "<script>window.location.href = '/admin/';</script>";
            die();
        } else {
            $error = "Fel lösenord.";
        }
    } else {
        $error = "Användare existerar inte.";
    }
} else {
    $error = "Vad gör du ens, sluta.";
}

if ($error != "none! :D") {
    $title = "Meescord - Fel :^)";
    $flex = false;
    require $_SERVER["DOCUMENT_ROOT"] . "/include/header.php";
    echo "<main id='lcontainer'><h1>Inloggnings fel!</h1><p>".$error."</p><a href='/admin/' class='btn btn-primary'>Gå tillbaka till inloggning</a></main>";
    require $_SERVER["DOCUMENT_ROOT"] . "/include/footer.php";
    die();
}

?>
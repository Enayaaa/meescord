<?php
if (isset($_COOKIE["cookie_allowed"])) {
    session_start();
    if (!isset($_SESSION["logged_in"]) || $_SESSION["admin"] != true) {
        die();
    }
} else {
    die();
}
if (isset($_POST["class"])) {
    $class = array();
    $class["title"] = $_POST["class"];
    $class["fname"] = "Generisk";
    $class["lname"] = "Användare";
    $class["img"] = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7";
    $class["rooms"] = array();
    $class["rooms"]["Rum 1"] = "Länk här";
    $class["subjects"] = array();
    $class["subjects"][0] = "Blah blah";
    $class["classes"] = array();
    $class["classes"]["Te1999999Z"] = array();
    $class["classes"]["Te1999999Z"]["info"] = "Inga länkar/instruktioner";
    $title = strtolower(str_replace(" ","",$_POST["class"]));
    file_put_contents($_SERVER["DOCUMENT_ROOT"]."/backend/classes/".$title.".json", json_encode($class));
    echo "<script>window.location.href = '/admin/';</script>";
} else {
    $title = "Meescord - Fel :^)";
    $flex = false;
    require $_SERVER["DOCUMENT_ROOT"] . "/include/header.php";
    echo "<main id='lcontainer'><h1>Skriv ett klassnamn!</h1><a href='/admin/' class='btn btn-primary'>Gå tillbaka.</a></main>";
    require $_SERVER["DOCUMENT_ROOT"] . "/include/footer.php";
    die();
}
?>
<?php
if (isset($_COOKIE["cookie_allowed"])) {
    session_start();
    $fix_cookie = false;
    if (!isset($_SESSION["logged_in"])) {
        $_SESSION["logged_in"] = false;
    }
} else {
    $fix_cookie = true;
}
$title = "Meescord - Admin";
$flex = false;
require $_SERVER["DOCUMENT_ROOT"] . "/include/header.php";
require $_SERVER["DOCUMENT_ROOT"] . "/admin/repair_secure.php";
?>
<main id="lcontainer">
    <h1>Lärare</h1>
    <?php
    if ($fix_cookie) {
        require $_SERVER["DOCUMENT_ROOT"] . "/admin/pages/cookie-allow.php";
    } else {
        if ($_SESSION["logged_in"] == "true") {
            require $_SERVER["DOCUMENT_ROOT"] . "/admin/pages/user.php";
        } else {
            require $_SERVER["DOCUMENT_ROOT"] . "/admin/pages/login.php";
        }
    }
    ?>
</main>
<?php require $_SERVER["DOCUMENT_ROOT"] . "/include/footer.php"; ?>
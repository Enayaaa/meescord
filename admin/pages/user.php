<h3>Hej <?php echo $_SESSION["name"]; ?>!</h3><br>
<?php
//todo: skapa nytt rum, ta bort rum
$users = json_decode(file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/admin/secure.json"), true);
$classesJson = (include $_SERVER["DOCUMENT_ROOT"] . "/backend/get_classes.php");
$classes = json_decode($classesJson, true);
?>


<!-- Inloggad som admin -->
<!--  -->
<!--  -->
<?php if ($_SESSION["admin"]) : ?>
<p>Skapa ny användare</p>
<form action="/admin/newuser.php" method="post" class="dumbborder">
    <div class="form-group">
        <label for="newuserfirstandlast">Förnamn och Efternamn</label>
        <input type="text" class="form-control" placeholder="Vad heter användaren?" name="firstandlast"
            id="newuserfirstandlast" required>
    </div>
    <div class="form-group">
        <label for="newuseruserpw">Lösenord till användaren</label>
        <input type="password" class="form-control" placeholder="Skriv användarens lösenord" name="userpw"
            id="newuseruserpw" required>
    </div>
    <div class="form-check">
        <input type="checkbox" class="form-check-input" name="isadmin" id="newuserisadmin">
        <label class="form-check-label" for="newuserisadmin">Användaren är administratör</label>
    </div>
    <button type="submit" class="btn btn-primary">Skapa ny användare</button>
</form>
<hr>
<p>Ändra användarpermissioner</p>
<form action="/admin/changeperms.php" method="post" class="dumbborder" id="changeperms">
    <div class="form-group">
        <label for="changepermsuser">Välj användare</label>
        <select class="form-control" id="changepermsuser" form="changeperms" name="user" required>
            <option>Välj en användare...</option>
            <?php foreach ($users as $key => $value) : ?>
            <option><?php echo $key; ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <p>Välj användarpermissioner</p>
    <div class="form-check">
        <?php foreach ($classes as $key => $value) : ?>
        <input type="checkbox" class="form-check-input" name="<?php echo $key ?>" id="<?php echo $value["title"] ?>">
        <label class="form-check-label" for="<?php echo $value["title"] ?>"><?php echo $value["title"] ?></label><br>
        <?php endforeach; ?>
    </div>
    <button type="submit" class="btn btn-primary">Ändra permissioner</button>
</form>
<hr>
<p>Byt till annan användare</p>
<form action="/admin/changeuser.php" method="post" class="dumbborder" id="changeuser">
    <div class="form-group">
        <label for="changeuseruser">Välj användare</label>
        <select class="form-control" id="changeuseruser" form="changeuser" name="user" required>
            <option>Välj en användare...</option>
            <?php foreach ($users as $key => $value) : ?>
            <option><?php echo $key; ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <p>Att ändra användare tvingar dig att temporärt ej vara admin, även om användaren du byter till är admin.<br>Logga
        ut och in igen för att komma tillbaka till din egna användare.</p>
    <button type="submit" class="btn btn-primary">Byt användare</button>
</form>
<hr>
<p>Lägg till klass</p>
<form action="/admin/addclass.php" method="post" class="dumbborder" id="addclass">
    <div class="form-group">
        <label for="addclassname">Klassnamn</label>
        <input type="text" class="form-control" placeholder="Klassnamn går här" name="class" id="addclassname" required>
    </div>
    <button type="submit" class="btn btn-primary">Skapa klass</button>
</form>
<hr>
<p>Ta bort klass</p>
<form action="/admin/deleteclass.php" method="post" class="dumbborder" id="deleteclass">
    <div class="form-group">
        <label for="deleteclasss">Välj klass</label>
        <select class="form-control" id="deleteclasss" form="deleteclass" name="class" required>
            <option>Välj en klass...</option>
            <?php foreach ($classes as $key => $value) : ?>
            <option><?php echo $value["filename"]; ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <button type="submit" class="btn btn-warning">Ta bort klass</button>
</form>
<hr>



<!-- Inloggad som användare -->
<!--  -->
<!--  -->
<?php else : ?>
<p>Klassadministering</p>
<?php foreach ($users[$_SESSION["name"]]["classes"] as $key => $value) : ?>
<?php $class = json_decode(file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/backend/classes/" . $key . ".json"), true); ?>
<form action="/admin/editclass.php?class=<?php echo $key; ?>" method="post" class="dumbborder"
    id="<?php echo $key . 'form'; ?>">
    <div class="form-group">
        <label for="<?php echo $key . 'title'; ?>">Klass titel</label>
        <input type="text" class="form-control" placeholder="Class title" name="title"
            id="<?php echo $key . 'title'; ?>" required value="<?php echo htmlspecialchars($class["title"]); ?>">
    </div>
    <div class="row form-group">
        <div class="col">
            <label for="<?php echo $key . 'fnamn'; ?>">Förnamn</label>
            <input type="text" class="form-control" placeholder="Förnamn" name="fnamn"
                id="<?php echo $key . 'fnamn'; ?>" required value="<?php echo htmlspecialchars($class["fname"]); ?>">
        </div>
        <div class="col">
            <label for="<?php echo $key . 'lnamn'; ?>">Efternamn</label>
            <input type="text" class="form-control" placeholder="Efternamn" name="lnamn"
                id="<?php echo $key . 'lnamn'; ?>" required value="<?php echo htmlspecialchars($class["lname"]); ?>">
        </div>
    </div>
    <div class="form-group">
        <label for="<?php echo $key . 'pic'; ?>">Bildlänk</label>
        <input type="text" class="form-control" placeholder="Bildlänk" name="pic" id="<?php echo $key . 'pic'; ?>"
            required value="<?php echo htmlspecialchars($class["img"]); ?>">
    </div>
    <div class="form-group">
        <label>Ämnen</label> <!-- i've sinned -->
        <div id="<?php echo $key . 'subjectdiv'; ?>">
            <?php foreach ($class["subjects"] as $keyy => $valuee) : ?>
            <input type="text" class="form-control" placeholder="..." name="subjects[]" required
                value="<?php echo htmlspecialchars($valuee); ?>">
            <br>
            <?php endforeach ?>
        </div>
        <button type="button" class="btn btn-secondary" onclick="addsubject('<?php echo $key . 'subjectdiv'; ?>')">Lägg
            till ämne</button>
        <button type="button" class="btn btn-danger" onclick="remove('<?php echo $key . 'subjectdiv'; ?>', 2)">Ta bort
            ämne</button>
    </div>
    <div class="form-group">
        <label>Klassinfo</label>
        <div id="<?php echo $key . 'classinfodiv'; ?>">
            <div class="row">
                <div class="col">
                    <label>Klassnamn</label>
                </div>
                <div class="col">
                    <label>Info</label>
                </div>
            </div>
            <?php foreach ($class["classes"] as $keyy => $valuee) : ?>
            <div class="row">
                <div class="col">
                    <input type="text" class="form-control" placeholder="..." name="classinfo1[]" required
                        value="<?php echo htmlspecialchars($keyy); ?>">
                </div>
                <div class="col">
                    <textarea form="<?php echo $key . 'form'; ?>" class="form-control" placeholder="..."
                        name="classinfo2[]" required><?php echo htmlspecialchars($valuee["info"]); ?></textarea>
                </div>
            </div>
            <br>
            <?php endforeach ?>
        </div>
        <button type="button" class="btn btn-secondary"
            onclick="addinfo('<?php echo $key . 'classinfodiv'; ?>', '<?php echo $key . 'form'; ?>')">Lägg till
            klassinfo</button>
        <button type="button" class="btn btn-danger" onclick="remove('<?php echo $key . 'classinfodiv'; ?>', 6)">Ta bort
            klassinfo</button>
    </div>
    <div class="form-group">
        <label>Meet-rum</label>
        <div id="<?php echo $key . 'meetroomdiv'; ?>">
            <div class="row">
                <div class="col">
                    <label>Rumsnamn</label>
                </div>
                <div class="col">
                    <label>Rumslänk</label>
                </div>
            </div>
            <?php foreach ($class["rooms"] as $keyy => $valuee) : ?>
            <div class="row">
                <div class="col">
                    <input type="text" class="form-control" placeholder="Rumsnamn" name="meetroom1[]" required
                        value="<?php echo htmlspecialchars($keyy); ?>">
                </div>
                <div class="col">
                    <input type="text" class="form-control" placeholder="Rumslänk" name="meetroom2[]" required
                        value="<?php echo htmlspecialchars($valuee); ?>">
                </div>
            </div>
            <br>
            <?php endforeach ?>
        </div>
        <button type="button" class="btn btn-secondary" onclick="addroom('<?php echo $key . 'meetroomdiv'; ?>')">Lägg
            till meet-rum</button>
        <button type="button" class="btn btn-danger" onclick="remove('<?php echo $key . 'meetroomdiv'; ?>', 6)">Ta bort
            meet-rum</button>
    </div>
    <button type="submit" class="btn btn-primary">Spara klass</button>
</form>
<br>
<?php endforeach; ?>
<hr>
<?php endif; ?>



<!-- Inställningar för både användare och admin -->
<p>Ändra lösenord</p>
<form action="/admin/pwchange.php" method="post" class="dumbborder">
    <div class="form-group">
        <label for="pwchangenewpw">Nytt lösenord</label>
        <input type="password" class="form-control" placeholder="Skriv ett nytt lösenord" id="pwchangenewpw"
            name="pwchangenewpw" required>
    </div>
    <div class="form-group">
        <label for="pwchangenewpw2">Nytt lösenord (igen)</label>
        <input type="password" class="form-control" placeholder="Skriv ett nytt lösenord (igen)" id="pwchangenewpw2"
            name="pwchangenewpw2" required>
    </div>
    <button type="submit" class="btn btn-danger">Byt lösenord</button>
</form>
<hr>
<p>Ta bort ditt konto</p>
<form action="/admin/delete.php" method="post" id="deleteuracc" class="dumbborder">
    <div class="form-group">
        <label for="deleteuraccpw">Ditt nuvarande lösenord</label>
        <input type="password" class="form-control" id="deleteuraccpw" placeholder="Skriv ditt nuvarande lösenord"
            name="deletepw" required>
    </div>
    <button type="submit" class="btn btn-danger" style="display: none;" id="deleteuraccbutton">(osynlig
        knapp)</button>
    <p>(Om du undrar, vi måste ha valet att ta bort konto p.g.a gpdr.)</p>
    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteUrAcc">Ta bort mitt
        konto!</button>
</form>
<hr>
<p>Klar för dagen?</p>
<a href="/admin/cookie_nuke.php" class="btn btn-secondary">Logga ut</a>

<div class="modal fade" id="deleteUrAcc" tabindex="-1" role="dialog" aria-labelledby="Delete ur acc lmao"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteuraccmodaltitle">Är du helt säker?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Du kommer inte kunna få tillbaka ditt konto!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Avbryt, för fan!</button>
                <button type="button" class="btn btn-danger" onclick="deleteuracc()">Ja, ta bort mitt
                    konto.</button>
            </div>
        </div>
    </div>
</div>

<script>
function deleteuracc() {
    if (document.getElementById("deleteuraccpw").value == "") {
        alert("Du skrev inte in ditt lösenord!");
    } else {
        document.getElementById("deleteuraccbutton").click();
    }
}

function addsubject(id) {
    var subject = document.createElement("input");
    subject.type = "text";
    subject.classList = "form-control";
    subject.placeholder = "Skriv ämnesnamn här";
    subject.name = "subjects[]";
    subject.required = true;
    document.getElementById(id).appendChild(subject);
    var br = document.createElement("br");
    document.getElementById(id).appendChild(br);
}

function addinfo(id, uwu) {
    var rowdiv = document.createElement("div");
    rowdiv.classList = "row";
    document.getElementById(id).appendChild(rowdiv);
    var coldiv1 = document.createElement("div");
    coldiv1.classList = "col";
    rowdiv.appendChild(coldiv1);
    var coldiv2 = document.createElement("div");
    coldiv2.classList = "col";
    rowdiv.appendChild(coldiv2);
    var classname = document.createElement("input");
    classname.type = "text";
    classname.classList = "form-control";
    classname.placeholder = "Skriv klassnamn här";
    classname.name = "classinfo1[]";
    classname.required = true;
    coldiv1.appendChild(classname);
    var classinfo = document.createElement("textarea");
    //classinfo.form = uwu; OKAY SO FOR SOME REASON YOU CANT DO THIS, but html5 standard says as long as textarea is in a <form> its fine, so . . . i guess it dont matter??
    classinfo.classList = "form-control";
    classinfo.placeholder = "Skriv klassinfo här";
    classinfo.name = "classinfo2[]";
    classinfo.required = true;
    coldiv2.appendChild(classinfo);
    var br = document.createElement("br");
    document.getElementById(id).appendChild(br);
}

function addroom(id) {
    var rowdiv = document.createElement("div");
    rowdiv.classList = "row";
    document.getElementById(id).appendChild(rowdiv);
    var coldiv1 = document.createElement("div");
    coldiv1.classList = "col";
    rowdiv.appendChild(coldiv1);
    var coldiv2 = document.createElement("div");
    coldiv2.classList = "col";
    rowdiv.appendChild(coldiv2);
    var roomname = document.createElement("input");
    roomname.type = "text";
    roomname.classList = "form-control";
    roomname.placeholder = "Skriv rumsnamn här";
    roomname.name = "meetroom1[]";
    roomname.required = true;
    coldiv1.appendChild(roomname);
    var roomlink = document.createElement("input");
    roomlink.type = "text";
    roomlink.classList = "form-control";
    roomlink.placeholder = "Skriv rumslänk här";
    roomlink.name = "meetroom2[]";
    roomlink.required = true;
    coldiv2.appendChild(roomlink);
    var br = document.createElement("br");
    document.getElementById(id).appendChild(br);
}

function remove(id, len) {
    var length = document.getElementById(id).getElementsByTagName("*").length;
    for (var i = 0; i < len; i++) {
        document.getElementById(id).getElementsByTagName("*")[length - (1 + i)].remove();
    }
}
</script>
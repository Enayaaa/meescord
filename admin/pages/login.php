<h3>Logga in som lärare</h3>
<form action="./login.php" method="post" id="loginform">
    <div class="form-group">
        <label for="user">Välj användare</label>
        <select class="form-control" id="user" form="loginform" name="user" required>
        <option>Välj en användare...</option>
        <?php $users = json_decode(file_get_contents($_SERVER["DOCUMENT_ROOT"]."/admin/secure.json"), true); ?>
        <?php foreach ($users as $key => $value) : ?>
            <option><?php echo $key; ?></option>
        <?php endforeach; ?>
        </select>
    </div>
    <div class="form-group">
        <label for="password">Lösenord</label>
        <input type="password" class="form-control" id="password" placeholder="Lösenord" name="password" required>
        <small class="form-text text-muted">Har du glömt ditt lösenord? Kontakta en administratör.</small>
    </div>
    <button type="submit" class="btn btn-primary">Logga in</button> <a href="#" class="btn btn-danger" onclick="nocookiesforu()">Ta bort kakor</a>
</div>
</form>

<script>
function nocookiesforu() {
    document.cookie = "cookie_allowed=;path=/;expires=Thu, 01 Jan 1970 00:00:00 GMT";
    window.location.href = "/admin/cookie_nuke.php";
}
</script>
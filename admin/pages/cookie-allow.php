<h3>Kakor</h3>
<p>För att kunna logga in som lärare måste du tillåta kakor. Är detta okej?</p>
<button type="button" class="btn btn-success" onclick="ok()">Ja</button> <button type="button" class="btn btn-danger" onclick="no()">Nej</button>
<script>
    function ok() {
        document.cookie = "cookie_allowed=true; path=/; expires=<?php echo date(DATE_COOKIE, time()+(365*24*60*60)); ?>"; //Thu, 18 Dec 2013 12:00:00 UTC
        location.reload();
    }
    function no() {
        alert("Surt för dig då");
        window.history.back();
    }
</script>
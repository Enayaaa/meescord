<?php
session_start(); // initialize session
session_destroy(); // destroy session
setcookie("PHPSESSID","",time()-3600,"/"); // delete session cookie
if (isset($_SERVER["HTTP_REFERER"])) {
    header("Location: " . $_SERVER["HTTP_REFERER"]);
} else {
    echo "<script>window.location.href = '/admin/';</script>";
}
die();
?>
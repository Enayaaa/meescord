<?php
if (isset($_COOKIE["cookie_allowed"])) {
    session_start();
    if (!isset($_SESSION["logged_in"]) || $_SESSION["admin"] != true) {
        die();
    }
} else {
    die();
}
if (isset($_POST["user"])) {
    $classesJson = (include $_SERVER["DOCUMENT_ROOT"] . "/backend/get_classes.php");
    $classes = json_decode($classesJson, true);
    $users = json_decode(file_get_contents($_SERVER["DOCUMENT_ROOT"]."/admin/secure.json"), true);
    $users[$_SESSION["name"]]["classes"] = array(); //reset the thing;
    foreach ($classes as $key => $value) {
        if (isset($_POST[$key]) && $_POST[$key] == true) {
            $users[$_POST["user"]]["classes"][$value["filename"]] = true;
        }
    }
    file_put_contents($_SERVER["DOCUMENT_ROOT"]."/admin/secure.json", json_encode($users));
    echo "<script>window.location.href = '/admin/';</script>";
    die();
} else {
    $title = "Meescord - Fel :^)";
    $flex = false;
    require $_SERVER["DOCUMENT_ROOT"] . "/include/header.php";
    echo "<main id='lcontainer'><h1>Användare finns inte.</h1><a href='/admin/' class='btn btn-primary'>Gå tillbaka.</a></main>";
    require $_SERVER["DOCUMENT_ROOT"] . "/include/footer.php";
    die();
}
?>
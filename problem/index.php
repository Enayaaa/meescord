<?php
$title = "Meescord - Problem";
$flex = false;
require $_SERVER["DOCUMENT_ROOT"] . "/include/header.php";
?>
    <main>
        <a href="/" class="btn btn-primary" role="button" style="margin-bottom: 1rem;">Tillbaka till
            kanaler</a>
        <h3 class="klasstitel">
            Länkar till Google Meet fungerar ej?
        </h3>
        <p>Om du är inloggad med flera google konton på din webbläsare kan ett problem förekomma...</p>
        <p>OBS! Du måste vara inloggad med din skolkonto på google i din webbläsare.</p>
        <img src="/assets/problem.JPG">
        <h4>Lösning</h4>
        <p>Klicka på rummet du vill gå till.</p>
        <p>Ändra siffran efter "authuser=" till "1" eller högre om du är inloggad med fler google konton i din
            webbläsare.</p>
        <img src="/assets/solution1.JPG">
        <br><br>
        <img src="/assets/solution2.JPG">
        <br><br><br>
        <h4>Fler problem?</h4>
        <p>Mejla till <a
                href="mailto:enanor1013@skola.goteborg.se?subject=Problem Meescord">enanor1013@skola.goteborg.se</a></p>

    </main>
<?php require $_SERVER["DOCUMENT_ROOT"] . "/include/footer.php"; ?>
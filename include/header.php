<?php 
if (isset($_COOKIE["cookie_allowed"])) {
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }
    if (isset($_SESSION["name"])) {
        $loginbutton = "Hej " . $_SESSION["name"] . "!";
    } else {
        $loginbutton = "Lärare";
    }
} else {
    $loginbutton = "Lärare";
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/assets/css/bootstrap-4-4-1.css">
    <!-- Bootstrap JavaScript -->
    <script type="text/javascript" src="/assets/js/jquery-3-4-1.js"></script>
    <script type="text/javascript" src="/assets/js/popper.js"></script>
    <script type="text/javascript" src="/assets/js/bootstrap.js"></script>
    <!-- Google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Arimo|Girassol&display=swap" rel="stylesheet">
    <!-- The rest lol -->
    <link rel="stylesheet" href="/style.css">
    <script type="text/javascript" src="/main.js"></script>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <title><?php echo $title; ?></title>
    <meta name="Description" content="Hitta nästa skolmöte!">
    <!-- misscord -->
    <?php if ($flex == false) {
        echo '<style>
    main {
        display: block;
        margin: 1rem;
    }
    </style>';
    } ?>
</head>

<body>
    <header>
        <a href="/" style="color: #000"><div class="logo">
            <img src="/favicon.ico" id="header-icon" alt="ikon">
            <h1 id="title">Meescord</h1>
        </div></a>
        <div class="btn-group links" role="group" aria-label="Basic example">
            <a rel="noreferrer" target="_blank" href="https://schema.lindholmen.club/" class="btn btn-secondary">Schema</a>
            <a rel="noreferrer" target="_blank" href="https://classroom.google.com/" class="btn btn-secondary">Classroom</a>
            <a rel="noreferrer" target="_blank" href="https://hjarntorget.goteborg.se/" class="btn btn-secondary">Hjärntorget</a>
            <a href="/admin/" class="btn btn-secondary"><?php echo $loginbutton ?></a>
        </div>
    </header>
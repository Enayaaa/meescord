<?php
$title = "Meescord";
$flex = true;
require $_SERVER["DOCUMENT_ROOT"] . "/include/header.php";
?>
    <main>
        <?php
        $classesJson = (include $_SERVER["DOCUMENT_ROOT"] . "/backend/get_classes.php");
        $classes = json_decode($classesJson, true);
        ?>

        <?php foreach ($classes as $key => $value) : ?>
        <div class="card teacher-card" style="width: 19rem;">
            <?php echo '<img src="' . $value['img'] . '" class="card-img-top" alt="' . $value['img'] . '">' ?>
            <div class="card-body">
                <h5 class="card-title"><?php echo $value["title"] ?></h5>
                <p class="card-text"><?php foreach ($value['subjects'] as $key) {
                                                echo $key . '<br>';
                                            } ?></p>
                <a href="kanaler/?class=<?php echo $value['filename'] ?>" class="btn btn-primary">Gå till
                    kanal</a>
            </div>
        </div>
        <?php endforeach; ?>
    </main>    
<?php require $_SERVER["DOCUMENT_ROOT"] . "/include/footer.php"; ?>